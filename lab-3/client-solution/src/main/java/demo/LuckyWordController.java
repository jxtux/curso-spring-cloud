package demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LuckyWordController {

	@Value("${lucky-word}")
	String luckyWord;

	@Value("${password:''}")
	String password;
	
	@Value("${pais:''}")
	String pais;

	@RequestMapping("/valoresConfig")
	public String showLuckyWord() {
		Map<String, String> dbMap = new HashMap<>();
		dbMap.put("valorLucky", luckyWord);
		dbMap.put("valorPassword", password);
		dbMap.put("pais", pais);

		return dbMap.toString();
	}
}
